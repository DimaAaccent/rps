import gulp       from 'gulp';

gulp.task('js', function () {
  return gulp.src('./app/assets/scripts/**/*.js')
    .pipe(gulp.dest('./app/dist/scripts/'))
});
