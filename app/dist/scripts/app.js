bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

var Navigation = (function() {
  function Navigation() {
    this.$dropdown = $('.dropdown');
    this.$dropdownMenu = $('.nav-menu-dropdown');
    this.$serviceLink = $('.service');
    this.$services = $('.services');
    this.$window = $(window);
    this.$body = $('html,body');

    this.dropdownCheck();
    this.bindEvents();
  }

  Navigation.prototype.bindEvents = function() {
    this.$window.on('resize', bind(this.dropdownCheck, this));
  }

  Navigation.prototype.dropdownCheck = function() {
    var width = this.$window.width();

    if (width < 640) {
      this.$dropdown.unbind('click');
      this.$dropdown.unbind('hover');
      this.$serviceLink.on('click', bind(this.scrollToService, this));
    } else {
      this.$dropdown.hover(
        function() {
          $('.nav-menu-dropdown', this).show();
          $(this).toggleClass('open');
        },
        function() {
          $('.nav-menu-dropdown', this).hide();
          $(this).toggleClass('open');
        }
      );
      this.$dropdown.on('click', this.preventClick);
    }
  }

  Navigation.prototype.preventClick = function(event) {
    event.preventDefault();
  }

  Navigation.prototype.scrollToService = function() {
    var height = this.$services.offset().top;

    this.$body.animate({
      scrollTop: height
    }, 100);
  }

  return Navigation;
})();

(function() {
  var $carousel = $('.carousel');
  var navigation = new Navigation();

  $(document).foundation();
  $('.fancybox').fancybox({
    helpers: {
      overlay: {
        locked: false
      }
    }
  });

  $carousel.slick({
    dots: false,
    arrows: false,
    autoplay: true
  });
})();
