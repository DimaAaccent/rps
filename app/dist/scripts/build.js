(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

bind = function bind(fn, me) {
  return function () {
    return fn.apply(me, arguments);
  };
};

function Navigation() {
  function Navigation() {
    this.$dropdown = $('.dropdown');
    this.$dropdownMenu = $('.nav-menu-dropdown');
    this.$serviceLink = $('.service');
    this.$services = $('.services');
    this.$window = $(window);
    this.$body = $('html,body');

    this.dropdownCheck();
    this.bindEvents();
  }

  Navigation.prototype.bindEvents = function () {
    this.$window.on('resize', bind(this.dropdownCheck, this));
  };

  Navigation.prototype.dropdownCheck = function () {
    var width = this.$window.width();

    if (width < 640) {
      this.$dropdown.unbind('click');
      this.$dropdown.unbind('hover');
      this.$serviceLink.on('click', bind(this.scrollToService, this));
    } else {
      this.$dropdown.hover(function () {
        $('.nav-menu-dropdown', this).show();
        $(this).toggleClass('open');
      }, function () {
        $('.nav-menu-dropdown', this).hide();
        $(this).toggleClass('open');
      });
      this.$dropdown.on('click', this.preventClick);
    }
  };

  Navigation.prototype.preventClick = function (event) {
    event.preventDefault();
  };

  Navigation.prototype.scrollToService = function () {
    var height = this.$services.offset().top;

    this.$body.animate({
      scrollTop: height
    }, 200);
  };
}

(function () {
  var $carousel = $('.carousel');
  var navigation = new Navigation();

  $carousel.slick({
    dots: false,
    arrows: false,
    autoplay: true
  });
})();

},{}]},{},[1]);
